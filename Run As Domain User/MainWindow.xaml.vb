﻿Imports System.IO, System.Windows, Microsoft.Win32

Class MainWindow
    Public ErrorList, AppPath As String

    Public Sub SetAppPath(Path As String)
        AppPath = Path
        Dim PathLength As Integer = Len(Path)
        Dim CroppedPathLength As Integer = 40 - Len(Split(Path, "\")(UBound(Split(Path, "\"))))
        REM Shorten if the path makes it too big for the textbox
        If PathLength > 40 Then
            Textbox_Application.Text = Path.Substring(0, CroppedPathLength) + "...\" + Split(Path, "\")(UBound(Split(Path, "\")))
        Else
            Textbox_Application.Text = Path
        End If

    End Sub

    Private Sub Textbox_Domain_GotFocus(sender As Object, e As RoutedEventArgs) Handles Textbox_Domain.GotFocus
        If Textbox_Domain.Text = "Domain" Then
            Textbox_Domain.Text = ""
        End If
    End Sub

    Private Sub Textbox_Username_GotFocus(sender As Object, e As RoutedEventArgs) Handles Textbox_Username.GotFocus
        If Textbox_Username.Text = "Username" Then
            Textbox_Username.Text = ""
        End If
    End Sub


    Private Sub Button_AppFind_Click(sender As Object, e As RoutedEventArgs) Handles Button_AppFind.Click
        Dim dlg As New OpenFileDialog()
        dlg.FileName = "Application"
        dlg.DefaultExt = ".exe"
        dlg.Filter = "Applications|*.exe"
        Dim dlgresult As Boolean = dlg.ShowDialog
        If dlgresult = True Then
            SetAppPath(dlg.FileName)
        End If
    End Sub

    Private Sub Button_Launch_Click(sender As Object, e As RoutedEventArgs) Handles Button_Launch.Click
        If Textbox_Username.Text = "Username" Or Textbox_Username.Text = "" Or Textbox_Domain.Text = "Domain" Or Textbox_Domain.Text = "" Or Textbox_Application.Text = "Choose application" Or Textbox_Application.Text = "" Then
            MessageBox.Show("Please complete all fields", "Error", vbOKOnly)
            Exit Sub
        End If
        Dim LaunchApp As New System.Diagnostics.Process
        Dim LaunchAppInfo As New System.Diagnostics.ProcessStartInfo
        LaunchAppInfo.FileName = "c:\windows\system32\runas.exe"
        LaunchAppInfo.Arguments = "/netonly /user:" + Textbox_Domain.Text + "\" + Textbox_Username.Text + " """ + AppPath + """"
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Pinchiesoft\RADU\", "domain", Textbox_Domain.Text)
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Pinchiesoft\RADU\", "username", Textbox_Username.Text)
        My.Computer.Registry.SetValue("HKEY_CURRENT_USER\Software\Pinchiesoft\RADU\", "application", AppPath)
        LaunchApp.StartInfo = LaunchAppInfo
        LaunchApp.Start()
    End Sub

    Private Sub Window_Initialized(sender As Object, e As EventArgs)
        If My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Pinchiesoft\RADU\", "domain", Nothing) <> "" Then
            Textbox_Domain.Text = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Pinchiesoft\RADU\", "domain", Nothing)
            Textbox_Username.Text = My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Pinchiesoft\RADU\", "username", Nothing)
            SetAppPath(My.Computer.Registry.GetValue("HKEY_CURRENT_USER\Software\Pinchiesoft\RADU\", "application", Nothing))
        End If
    End Sub
End Class
